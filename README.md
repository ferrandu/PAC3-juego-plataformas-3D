# PAC3-juego-plataformas-3D
Entrega PAC3 - Programación Unity3D - UOC

Juego de Plataformas 3D estilo cartoon en el que el usuario deberá enfrentarse a una apocalipsis Zombie y rescatar a sus compañeros perdidos. El jugador podrá recoger objetos de vida y munición. 
El escenario, la ambientación y la música estan inspirados a juegos como el Metal Slug y al Time Crisis.

![render_menu](https://user-images.githubusercontent.com/49591290/58749039-120cff00-8481-11e9-86d5-df47db8fe749.jpg)


# Como jugar
Descargar y extraer el fichero.zip y ejecutar el fichero "Zombies_Party.exe" dentro de la carperta Build. 


# Controles de juego
Para el movimiento del player se pueden usar las flechas o las teclas WASD.

Para saltar pulsar la "barra espaciadora".

Para apuntar con arma y giro del personaje utilizar desplazamiento del ratón.

Para disparar arma pulsar tecla izquierda del ratón o la tecla "Control/Ctrl" derecho.

Pulsar la tecla "Esc" para ir al menú principal.

![RenderJuego2](https://user-images.githubusercontent.com/49591290/58749109-ce66c500-8481-11e9-876a-422d9c022039.jpg)





# Puntos básicos de programación que se han implementado
1. **Player**
*  Se ha implementado el Script **PlayerMovement** para el movimiento del personaje en tercera persona. Se han asociado las animaciones de correr y salto del personaje
*  a los inputs vertical/horizontal y barra espaciadora. Para el giro y rotación del personaje se utilizado la posición del ratón en la pantalla. 
*  Script **PlayerShooting** para el disparo con el arma. Tambien va asociado a la animación de disparo del personaaje. En este caso se han utilizado integer para el numero 
*  de balas, daño de cada disparo y rango de disparo. Este Script también indica el número de munición que le queda al personaje en el canvas y va atachado a la arma del pesonaje.
*  Script **PlayerHealth** para tener la vida del player y recibir daño/vida. En este caso se le han añadido partículas cuando el personaje recibe daño al igual que la animación
*  de morir cuando la vida llega a 0. Se ha tenido en cuenta el slider de unity para la barra de vida del personaje. La cantidad de vida también va medida por integers.

2. **Enemigos**
*  Para la implementación de los zombies enemigos se han tenido en cuenta varios Scripts con funcionalidades distintas:
*  Script **EnemyMovement** se ha utilizado para cada uno de los enemigos y controla su movimiento en la escena. Éste script tiene en cuenta la posición (transformada) del player
*  para que los enemigos vayan hacia él y así poder atacarlo. El Script recoge el componente NavMeshAgent de cada uno de los enemigos y en caso que la vida del player llegue a zero,
*  dejan de perseguirle.
*  Script **Enemy Attack** se utiliza para activar el ataque de cada uno de los enemigos, mediante trigger junto con las animaciones correspondientes. En éste script también se han
*  usado integers para establecer el daño de cada uno de los diferentes enemigos y así hacer algunos más fuertes y poderosos que otros. 
*  Script **EnemyHealth** se utiliza para controlar el nivel de vida de cada uno de los enemigos junto a la animación de morir. En ellos se han utilizado integers para cambiar los 
*  valores de cada uno y detectar si el enemigo ha muerto. En este script también se ha implementado una función que destruye y hace desaparecer el enemigo para mejorar el rendimiento 
*  del juego.
*  Los Scripts **EnemyManager** y **ActivateSpawnZombies** se utilizan solamente en la parte final del juego para activar dos "spawn points" de enemigos cuando el player conigue salvar 
*  a todos sus compañeros. Éste script provoca que el player vaya hacia la "area de seguridad" y completar el juego ya que no podria escapar de la invasión de zombies que se le avecina. 

3. **Otros Scripts que se han implementado**
*  En esta parte se ha utilizado varios Scripts para la obtención de munición, vida, activación de hordas de enemigos, explosiones y soldados aliados rescatados.
*  Scripts **GetAmmo** y **GetHealth** se encargan de rellenar la munición total y restaurar la barra de vida del Player al detectar su colision con el objeto asociado. 
*  Script **CameraFollow** se encarga de seguir el movimiento del Player por toda la escena.
*  El Script **ExplosionVehicle** se encarga de eliminar algunos de los objetos de la escena y reproducir sus partículas y audios asociados. Son los objetos que dificultan y obstruyen
*  el paso para salvar a los compañeros y completar la misión.
*  Los Scripts **SoldiersMovement** y **SoldiersInSafeArea** se encargan de comprobar si el Player ha colisionado con cada uno de los 5 compañeros a rescatar y así poder activar su
*  animación y áudio. éstos personajes también tienen asociado una NavMeshAgent asociada a la posición inicial del juego con el camión "Safety Area". También se indica por imagen en el 
*  canvas cada uno de los soldados/compañeros que llegan al safety area. 
*  El Script **ActivateZombiesHord** se ha utlizado para activar dieferentes grupos de zombies mediante trigger en diferentes zonas del escenario del juego.

4. **Controles de pantallas**
*  Por lo que refiere a la navegación de las pantallas del Menu y Final de juego se ha utilizado el Script **MenuControls**.
*  Se ha utilizado el Script **GameOverManager** para la gestión de la pantalla de game over dentro del juego activandola mediante animación.
*  El Script **ScoreManager** se encarga de establecer el número de zombies que el personaje consigue matar en el juego.
 

# Puntos extra que se han implementado
1. **Diferentes tipos de enemigos**: Durante el juego aparecen muchos tipos de zombies distintos con animaciones diferentes que comparten movimientos, vida y ataque. Así pues se pueden
distinguir los zombies normales que se mueren a los 5 disparos, los zombies infectados con pretuberancias que són más difíciles de matar (más disparos). También se ha creado un Boss
zombie que sólo aparece en una parte del escenario y es el enemigo más fuerte anivel de vida y de ataque.
2. **SpawnZombies**: Se ha optado por utilizar un sistema de "Spawneo" de zombies para que se vayan generando cada segundo y así hacerle impossible al player derrotar a 
todos los zombies y buscar la escapatoria al "safety area".
3. **Bluesquad Soldiers**: Para mejorar la jugabilidad se ha implentado como misión principal para poder completar el juego el hecho de que el player tenga que encontrar a sus compañeros
de equipo perdidos por el escenario dando un toque extra de heroísmo y compañerismo a la sensación de juego. 

![RenderJuego3](https://user-images.githubusercontent.com/49591290/58750141-d11be700-848e-11e9-90dd-d13308f89a46.jpg)

4. **Musicas y Efectos de Sonido**: Para generar la ambientación de acción y aventura que se buscava desde un incio se han utilizado soundtracks del juego Metal Slug 3 y efectos de sonido
 parecidos tanto en las pantallas de menú y controles como en la pantalla de juego. Hay que tener en cuenta que los audios de las voces de cada uno de los personajes se han creado con mi
propia voz para así conseguir el efecto deseado.
5. **Partículas y Skybox**: Como aspectos visuales se ha optado de canviar la default Skybox de Unity por otra Skybox con cielo de atardecer más adiente para el juego al igual que
se ha generado varios sistemas de partículas para crear explosiones cuando se destruyen los vehiculos de la escena. Al igual que se han generado partículas para las salpicaduras y los
disparos. Las partículas ayudan a generar más dinamismo y immersión en el juego.

# Link al video de gameplay del juego y comentarios de lo que se ha implementado
[Video Gameplay Zombies Party (Juego Plataformas 3D)](https://www.youtube.com/watch?v=8LcsFndnYU0&t=3s)

